QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = artesky-flat
TEMPLATE = app

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/assistants/asteropeassistantswidget2.cpp \
    src/assistants/asteropeassistantswidget.cpp \
    src/camera/asteropecamerawidget2.cpp \
    src/camera/asteropecamerawidget.cpp \
    src/asteropestyle.cpp \
    src/settings/settingsdiagnostics.cpp \
    src/settings/settingsconfigure.cpp \
    src/settings/settingslanguage.cpp \
    src/settings/settingssequence.cpp \
    src/settings/settingssetup.cpp \
    src/settings/settingsusbkey.cpp \
    src/asteropeblackwidget.cpp \
    src/about/asteropeaboutuswidget.cpp \
    src/asterope.cpp \
    src/asteropemainwidget.cpp \
    src/asteropestylesheet.cpp \
    src/main.cpp \
    src/sequence/asteropesequencewidget.cpp \
    src/settings/asteropesettingswidget.cpp

HEADERS += \
    include/assistants/asteropeassistantswidget2.h \
    include/assistants/asteropeassistantswidget.h \
    include/camera/asteropecamerawidget2.h \
    include/camera/asteropecamerawidget.h \
    include/asteropestyle.h \
    include/settings/settingsdiagnostics.h \
    include/settings/settingsconfigure.h \
    include/settings/settingslanguage.h \
    include/settings/settingssequence.h \
    include/settings/settingssetup.h \
    include/settings/settingsusbkey.h \
    include/asteropeblackwidget.h \
    include/about/asteropeaboutuswidget.h \
    include/asteropeenum.h \
    include/asterope.h \
    include/asteropeconst.h \
    include/asteropemainwidget.h \
    include/sequence/asteropesequencewidget.h \
    include/settings/asteropesettingswidget.h

FORMS += \
    forms/about/asteropeaboutuswidget.ui \
    forms/assistants/asteropeassistantswidget.ui \
    forms/assistants/asteropeassistantswidget2.ui \
    forms/asterope.ui \
    forms/asteropeblackwidget.ui \
    forms/asteropemainwidget.ui \
    forms/sequence/asteropesequencewidget.ui \
    forms/camera/asteropecamerawidget.ui \
    forms/camera/asteropecamerawidget2.ui \
    forms/settings/asteropesettingswidget.ui \
    forms/settings/settingsconfigure.ui \
    forms/settings/settingsdiagnostics.ui \
    forms/settings/settingslanguage.ui \
    forms/settings/settingssequence.ui \
    forms/settings/settingssetup.ui \
    forms/settings/settingsusbkey.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DEFINES += APP_NAME=\\\"Asterope\\\"
VERSION = 0.1.0
DEFINES += APP_VERSION=\\\"$$VERSION\\\"

DISTFILES += \
    artesky-flat-gui.pri \
    asterope.pri

include(asterope.pri)

RESOURCES += \
    asterope.qrc
