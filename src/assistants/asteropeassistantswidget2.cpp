#include "../../include/assistants/asteropeassistantswidget2.h"
#include "ui_asteropeassistantswidget2.h"
#include <QDebug>

AsteropeAssistantsWidget2::AsteropeAssistantsWidget2(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AsteropeAssistantsWidget2)
{
    ui->setupUi(this);
    initAsteropeAssistantsMainWidget();
}

AsteropeAssistantsWidget2::~AsteropeAssistantsWidget2()
{
    delete ui;
}

void AsteropeAssistantsWidget2::resizeEvent(QResizeEvent *)
{
    initAsteropeAssistantsMainWidget();
}

void AsteropeAssistantsWidget2::showEvent(QShowEvent *)
{
    initAsteropeAssistantsMainWidget();
}

void AsteropeAssistantsWidget2::initAsteropeAssistantsMainWidget()
{
    QSize _buttonSize;
    _buttonSize = ui->assistantsFocusButton->size();
    int _w = static_cast<int>(_buttonSize.width()*0.8);
    int _h = static_cast<int>(_buttonSize.height()*0.8);
    int _f = static_cast<int>(_buttonSize.height()*0.1);
    QSize _size = QSize(_w,_h);
    ui->assistantsFocusButton->setIconSize(_size);
    ui->assistantsReportButton->setIconSize(_size);
    ui->assistantsExportDataButton->setIconSize(_size);
    ui->assistantsPolarButton->setIconSize(_size);

    ui->assistantsFocusLabel->setStyleSheet(this->getAsteropeAssistantsLabelStyle());
    ui->assistantsReportLabel->setStyleSheet(this->getAsteropeAssistantsLabelStyle());
    ui->assistantsExportDataLabel->setStyleSheet(this->getAsteropeAssistantsLabelStyle());
    ui->assistantsPolarLabel->setStyleSheet(this->getAsteropeAssistantsLabelStyle());

    ui->assistantsFocusLabel->setText(tr("FOCUS"));
    ui->assistantsReportLabel->setText(tr("SAVE REPORT"));
    ui->assistantsExportDataLabel->setText(tr("EXPORT DATA"));
    ui->assistantsPolarLabel->setText(tr("POLAR ALIGN"));

    QFont _font = ui->assistantsFocusLabel->font();
    _font.setPixelSize(_f);

    ui->assistantsFocusLabel->setFont(_font);
    ui->assistantsReportLabel->setFont(_font);
    ui->assistantsExportDataLabel->setFont(_font);
    ui->assistantsPolarLabel->setFont(_font);

    ui->asteropeAssistantsWidget->setStyleSheet(this->getAsteropeAssistantsWidgetStyle());
    ui->assistantsFocusButton->setStyleSheet(this->getAsteropeAssistantsButtonStyle());
    ui->assistantsReportButton->setStyleSheet(this->getAsteropeAssistantsButtonStyle());
    ui->assistantsExportDataButton->setStyleSheet(this->getAsteropeAssistantsButtonStyle());
    ui->assistantsPolarButton->setStyleSheet(this->getAsteropeAssistantsButtonStyle());
}
