#include "../../include/assistants/asteropeassistantswidget.h"
#include "ui_asteropeassistantswidget.h"
#include <QDebug>

AsteropeAssistantsWidget::AsteropeAssistantsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AsteropeAssistantsWidget)
{
    ui->setupUi(this);
    initAsteropeAssistantsMainWidget();
}

AsteropeAssistantsWidget::~AsteropeAssistantsWidget()
{
    delete ui;
}

void AsteropeAssistantsWidget::resizeEvent(QResizeEvent *)
{
    initAsteropeAssistantsMainWidget();
}

void AsteropeAssistantsWidget::showEvent(QShowEvent *)
{
    initAsteropeAssistantsMainWidget();
}

void AsteropeAssistantsWidget::initAsteropeAssistantsMainWidget()
{
    QSize _buttonSize;
    _buttonSize = ui->assistantsBiasButton->size();
    int _w = static_cast<int>(_buttonSize.width()*0.8);
    int _h = static_cast<int>(_buttonSize.height()*0.8);
    int _f = static_cast<int>(_buttonSize.height()*0.1);
    QSize _size = QSize(_w,_h);
    ui->assistantsBiasButton->setIconSize(_size);
    ui->assistantsDarkButton->setIconSize(_size);
    ui->assistantsFlatButton->setIconSize(_size);
    ui->assistantsDSButton->setIconSize(_size);
    ui->assistantsStartrailsButton->setIconSize(_size);
    ui->assistantsNextButton->setIconSize(_size);

    ui->assistantsBiasLabel->setStyleSheet(this->getAsteropeAssistantsLabelStyle());
    ui->assistantsDarkLabel->setStyleSheet(this->getAsteropeAssistantsLabelStyle());
    ui->assistantsFlatLabel->setStyleSheet(this->getAsteropeAssistantsLabelStyle());
    ui->assistantsDSLabel->setStyleSheet(this->getAsteropeAssistantsLabelStyle());
    ui->assistantsStartrailsLabel->setStyleSheet(this->getAsteropeAssistantsLabelStyle());
    ui->assistantsNextLabel->setStyleSheet(this->getAsteropeAssistantsCtrlLabelStyle());

    ui->assistantsBiasLabel->setText(tr("BIAS"));
    ui->assistantsDarkLabel->setText(tr("DARK"));
    ui->assistantsFlatLabel->setText(tr("FLAT"));
    ui->assistantsDSLabel->setText(tr("DEEPSKY"));
    ui->assistantsStartrailsLabel->setText(tr("STAR TRAILS"));
    ui->assistantsNextLabel->setText(tr("MORE"));

    QFont _font = ui->assistantsBiasLabel->font();
    _font.setPixelSize(_f);

    ui->assistantsBiasLabel->setFont(_font);
    ui->assistantsDarkLabel->setFont(_font);
    ui->assistantsFlatLabel->setFont(_font);
    ui->assistantsDSLabel->setFont(_font);
    ui->assistantsStartrailsLabel->setFont(_font);
    ui->assistantsNextLabel->setFont(_font);

    ui->asteropeAssistantsWidget->setStyleSheet(this->getAsteropeAssistantsWidgetStyle());
    ui->assistantsBiasButton->setStyleSheet(this->getAsteropeAssistantsButtonStyle());
    ui->assistantsDarkButton->setStyleSheet(this->getAsteropeAssistantsButtonStyle());
    ui->assistantsFlatButton->setStyleSheet(this->getAsteropeAssistantsButtonStyle());
    ui->assistantsDSButton->setStyleSheet(this->getAsteropeAssistantsButtonStyle());
    ui->assistantsStartrailsButton->setStyleSheet(this->getAsteropeAssistantsButtonStyle());
    ui->assistantsNextButton->setStyleSheet(this->getAsteropeAssistantsCtrlButtonStyle());
}

void AsteropeAssistantsWidget::on_assistantsNextButton_pressed()
{
    qDebug() << "Assistants | Next button pressed";
    emit asteropeAssistantsMenuButton_pressed(ASSISTANTS_SECOND_PAGE);
}
