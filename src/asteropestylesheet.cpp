#include "../include/asterope.h"
#include "../include/asteropemainwidget.h"
#include "../include/asteropestyle.h"

QString Asterope::getCtrlStatusMainWidgetStyle()
{
    QString _style = "QWidget{"
                     "background-color: rgb(53, 47, 55);}";
    return _style;
}

QString Asterope::getCtrlCmdMainWidgetStyle()
{
    QString _style = "QWidget{"
                     "background-color: rgb(0, 0, 0);}";
    return _style;
}

QString AsteropeMainWidget::getAsteropeMainWidgetStyle()
{
    QString _style = "QWidget{"
                     "background-color: rgb(53, 47, 55);}";
    return _style;
}

QString AsteropeAboutUsWidget::getAsteropeAboutUsWidgetStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsWidgetStyle(ASTEROPE_ABOUTUS);
}

QString AsteropeAboutUsWidget::getAsteropeAboutUsButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_ABOUTUS);
}

QString AsteropeAboutUsWidget::getAsteropeAboutUsLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_ABOUTUS);
}

QString AsteropeAssistantsWidget::getAsteropeAssistantsWidgetStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsWidgetStyle(ASTEROPE_ASSISTANTS);
}

QString AsteropeAssistantsWidget::getAsteropeAssistantsButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_ASSISTANTS);
}

QString AsteropeAssistantsWidget::getAsteropeAssistantsLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_ASSISTANTS);
}

QString AsteropeAssistantsWidget::getAsteropeAssistantsCtrlButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_CTRL);
}

QString AsteropeAssistantsWidget::getAsteropeAssistantsCtrlLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_CTRL);
}

QString AsteropeAssistantsWidget2::getAsteropeAssistantsWidgetStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsWidgetStyle(ASTEROPE_ASSISTANTS);
}

QString AsteropeAssistantsWidget2::getAsteropeAssistantsButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_ASSISTANTS);
}

QString AsteropeAssistantsWidget2::getAsteropeAssistantsLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_ASSISTANTS);
}

QString AsteropeAssistantsWidget2::getAsteropeAssistantsCtrlButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_CTRL);
}

QString AsteropeAssistantsWidget2::getAsteropeAssistantsCtrlLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_CTRL);
}

QString AsteropeCameraWidget::getAsteropeCameraWidgetStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsWidgetStyle(ASTEROPE_CAMERA);
}

QString AsteropeCameraWidget::getAsteropeCameraButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_CAMERA);
}

QString AsteropeCameraWidget::getAsteropeCameraLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_CAMERA);
}

QString AsteropeCameraWidget::getAsteropeCameraCtrlButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_CTRL);
}

QString AsteropeCameraWidget::getAsteropeCameraCtrlLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_CTRL);
}

QString AsteropeCameraWidget2::getAsteropeCameraWidgetStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsWidgetStyle(ASTEROPE_CAMERA);
}

QString AsteropeCameraWidget2::getAsteropeCameraButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_CAMERA);
}

QString AsteropeCameraWidget2::getAsteropeCameraLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_CAMERA);
}

QString AsteropeCameraWidget2::getAsteropeCameraCtrlButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_CTRL);
}

QString AsteropeCameraWidget2::getAsteropeCameraCtrlLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_CTRL);
}

QString AsteropeSequenceWidget::getAsteropeSequenceWidgetStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsWidgetStyle(ASTEROPE_SEQUENCE);
}

QString AsteropeSettingsWidget::getAsteropeSettingsWidgetStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsWidgetStyle(ASTEROPE_SETTINGS);
}

QString AsteropeSettingsWidget::getAsteropeSettingsButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_SETTINGS);
}

QString AsteropeSettingsWidget::getAsteropeSettingsLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_SETTINGS);
}

QString SettingsLanguage::getAsteropeLanguageWidgetStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsWidgetStyle(ASTEROPE_SETTINGS);
}

QString SettingsLanguage::getAsteropeLanguageButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_SETTINGS);
}

QString SettingsLanguage::getAsteropeLanguageLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_SETTINGS);
}

QString SettingsConfigure::getAsteropeConfigureWidgetStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsWidgetStyle(ASTEROPE_SETTINGS);
}

QString SettingsConfigure::getAsteropeConfigureButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_SETTINGS);
}

QString SettingsConfigure::getAsteropeConfigureLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_SETTINGS);
}

QString SettingsSequence::getAsteropeSettingsSequenceWidgetStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsWidgetStyle(ASTEROPE_SETTINGS);
}

QString SettingsSequence::getAsteropeSettingsSequenceButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_SETTINGS);
}

QString SettingsSequence::getAsteropeSettingsSequenceLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_SETTINGS);
}

QString SettingsUsbKey::getAsteropeUsbWidgetStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsWidgetStyle(ASTEROPE_SETTINGS);
}

QString SettingsUsbKey::getAsteropeUsbButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_SETTINGS);
}

QString SettingsUsbKey::getAsteropeUsbLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_SETTINGS);
}

QString SettingsSetup::getAsteropeSetupWidgetStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsWidgetStyle(ASTEROPE_SETTINGS);
}

QString SettingsSetup::getAsteropeSetupButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_SETTINGS);
}

QString SettingsSetup::getAsteropeSetupLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_SETTINGS);
}

QString SettingsDiagnostics::getAsteropeSettingsDiagnosticsWidgetStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsWidgetStyle(ASTEROPE_SETTINGS);
}

QString SettingsDiagnostics::getAsteropeSettingsDiagnosticsButtonStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsButtonStyle(ASTEROPE_SETTINGS);
}

QString SettingsDiagnostics::getAsteropeSettingsDiagnosticsLabelStyle()
{
    AsteropeStyle _asteropeStyle;
    return _asteropeStyle.getSettingsLabelStyle(ASTEROPE_SETTINGS);
}

QString AsteropeMainWidget::getAsteropeButtonStyle(AsteropeSection section)
{
    QString _style = "";
    AsteropeStyle _asteropeStyle;
    switch(section)
    {
    case ASTEROPE_SETTINGS:
        _style = _asteropeStyle.getSettingsButtonStyle(ASTEROPE_SETTINGS);
        break;
    case ASTEROPE_CAMERA:
        _style = _asteropeStyle.getSettingsButtonStyle(ASTEROPE_CAMERA);
        break;
    case ASTEROPE_ASSISTANTS:
        _style = _asteropeStyle.getSettingsButtonStyle(ASTEROPE_ASSISTANTS);
        break;
    case ASTEROPE_SEQUENCE:
        _style = _asteropeStyle.getSettingsButtonStyle(ASTEROPE_SEQUENCE);
        break;
    case ASTEROPE_ABOUTUS:
        _style = _asteropeStyle.getSettingsButtonStyle(ASTEROPE_ABOUTUS);
        break;
    case ASTEROPE_EXIT:
        _style = _asteropeStyle.getSettingsButtonStyle(ASTEROPE_EXIT);
        break;
    case ASTEROPE_CTRL:
        _style = _asteropeStyle.getSettingsButtonStyle(ASTEROPE_CTRL);
        break;
    }
    return _style;
}

QString AsteropeMainWidget::getAsteropeLineStyle(AsteropeSection section)
{
    QString _style = "";
    switch(section)
    {
    case ASTEROPE_SETTINGS:
        _style = "border-width: 1px;"
        "border-style: solid;"
        "background-color: #d14a50;"
        "border-color: black;";
        break;
    case ASTEROPE_CAMERA:
        _style = "border-width: 1px;"
        "border-style: solid;"
        "background-color: #3bd472;"
        "border-color: black;";
        break;
    case ASTEROPE_ASSISTANTS:
        _style = "border-width: 1px;"
        "border-style: solid;"
        "background-color: #eec608;"
        "border-color: black;";
        break;
    case ASTEROPE_SEQUENCE:
        _style = "border-width: 1px;"
        "border-style: solid;"
        "background-color: #1c95e9;"
        "border-color: black;";
        break;
    case ASTEROPE_ABOUTUS:
        _style = "border-width: 1px;"
        "border-style: solid;"
        "background-color: #fd7822;"
        "border-color: black;";
        break;
    case ASTEROPE_EXIT:
        _style = "border-width: 1px;"
        "border-style: solid;"
        "background-color: #8843a9;"
        "border-color: black;";
        break;
    case ASTEROPE_CTRL:
        _style = "border-width: 1px;"
        "border-style: solid;"
        "background-color: black;"
        "border-color: black;";
        break;
    }
    return _style;
}
