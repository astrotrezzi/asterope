#include "../../include/sequence/asteropesequencewidget.h"
#include "ui_asteropesequencewidget.h"

AsteropeSequenceWidget::AsteropeSequenceWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AsteropeSequenceWidget)
{
    ui->setupUi(this);
    ui->asteropeSequenceWidget->setStyleSheet(this->getAsteropeSequenceWidgetStyle());
}

AsteropeSequenceWidget::~AsteropeSequenceWidget()
{
    delete ui;
}
