#include "..\include\asteropeblackwidget.h"
#include "ui_asteropeblackwidget.h"

AsteropeBlackWidget::AsteropeBlackWidget(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AsteropeBlackWidget)
{
    ui->setupUi(this);
}

AsteropeBlackWidget::~AsteropeBlackWidget()
{
    delete ui;
}

void AsteropeBlackWidget::resizeEvent(QResizeEvent *)
{
    initAsteropeBlackWidget();
}

void AsteropeBlackWidget::showEvent(QShowEvent *)
{
    initAsteropeBlackWidget();
}

void AsteropeBlackWidget::initAsteropeBlackWidget()
{
    QSize _buttonSize;
    _buttonSize = ui->switchOnButton->size();
    ui->switchOnButton->setIconSize(QSize(static_cast<int>(_buttonSize.width()*0.8),static_cast<int>(_buttonSize.height()*0.8)));
}

void AsteropeBlackWidget::on_switchOnButton_pressed()
{
    this->hide();
}
