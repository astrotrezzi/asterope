#include "../../include/camera/asteropecamerawidget2.h"
#include "ui_asteropecamerawidget2.h"
#include <QDebug>

AsteropeCameraWidget2::AsteropeCameraWidget2(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AsteropeCameraWidget2)
{
    ui->setupUi(this);
    initAsteropeCameraMainWidget();
}

AsteropeCameraWidget2::~AsteropeCameraWidget2()
{
    delete ui;
}

void AsteropeCameraWidget2::showEvent(QShowEvent *)
{
    initAsteropeCameraMainWidget();
}

void AsteropeCameraWidget2::resizeEvent(QResizeEvent *)
{
    initAsteropeCameraMainWidget();
}

void AsteropeCameraWidget2::initAsteropeCameraMainWidget()
{
    QSize _buttonSize;
    _buttonSize = ui->cameraFileFormatButton->size();
    int _w = static_cast<int>(_buttonSize.width()*0.8);
    int _h = static_cast<int>(_buttonSize.height()*0.8);
    int _f = static_cast<int>(_buttonSize.height()*0.1);
    QSize _size = QSize(_w,_h);
    ui->cameraFileFormatButton->setIconSize(_size);
    ui->cameraWBButton->setIconSize(_size);
    ui->cameraColorSpaceButton->setIconSize(_size);
    ui->cameraDriveModeButton->setIconSize(_size);

    ui->cameraFileFormatLabel->setStyleSheet(this->getAsteropeCameraLabelStyle());
    ui->cameraWBLabel->setStyleSheet(this->getAsteropeCameraLabelStyle());
    ui->cameraColorSpaceLabel->setStyleSheet(this->getAsteropeCameraLabelStyle());
    ui->cameraDriveModeLabel->setStyleSheet(this->getAsteropeCameraLabelStyle());

    ui->cameraFileFormatLabel->setText(tr("FILE FORMAT"));
    ui->cameraWBLabel->setText(tr("WHITE BALANCE"));
    ui->cameraColorSpaceLabel->setText(tr("COLOR SPACE"));
    ui->cameraDriveModeLabel->setText(tr("DRIVE MODE"));

    QFont _font = ui->cameraFileFormatLabel->font();
    _font.setPixelSize(_f);

    ui->cameraFileFormatLabel->setFont(_font);
    ui->cameraWBLabel->setFont(_font);
    ui->cameraColorSpaceLabel->setFont(_font);
    ui->cameraDriveModeLabel->setFont(_font);

    ui->asteropeCameraWidget->setStyleSheet(this->getAsteropeCameraWidgetStyle());
    ui->cameraFileFormatButton->setStyleSheet(this->getAsteropeCameraButtonStyle());
    ui->cameraWBButton->setStyleSheet(this->getAsteropeCameraButtonStyle());
    ui->cameraColorSpaceButton->setStyleSheet(this->getAsteropeCameraButtonStyle());
    ui->cameraDriveModeButton->setStyleSheet(this->getAsteropeCameraButtonStyle());
}
