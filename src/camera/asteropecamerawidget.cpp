#include "../../include/camera/asteropecamerawidget.h"
#include "ui_asteropecamerawidget.h"
#include <QDebug>

AsteropeCameraWidget::AsteropeCameraWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AsteropeCameraWidget)
{
    ui->setupUi(this);
    initAsteropeCameraMainWidget();
}

AsteropeCameraWidget::~AsteropeCameraWidget()
{
    delete ui;
}

void AsteropeCameraWidget::resizeEvent(QResizeEvent *)
{
    initAsteropeCameraMainWidget();
}

void AsteropeCameraWidget::showEvent(QShowEvent *)
{
    initAsteropeCameraMainWidget();
}

void AsteropeCameraWidget::initAsteropeCameraMainWidget()
{
    QSize _buttonSize;
    _buttonSize = ui->cameraCaptureButton->size();
    int _w = static_cast<int>(_buttonSize.width()*0.8);
    int _h = static_cast<int>(_buttonSize.height()*0.8);
    int _f = static_cast<int>(_buttonSize.height()*0.1);
    QSize _size = QSize(_w,_h);
    ui->cameraCaptureButton->setIconSize(_size);
    ui->cameraExposureButton->setIconSize(_size);
    ui->cameraIsoButton->setIconSize(_size);
    ui->cameraApertureButton->setIconSize(_size);
    ui->cameraConnectButton->setIconSize(_size);
    ui->cameraNextButton->setIconSize(_size);

    ui->cameraCaptureLabel->setStyleSheet(this->getAsteropeCameraLabelStyle());
    ui->cameraExposureLabel->setStyleSheet(this->getAsteropeCameraLabelStyle());
    ui->cameraIsoLabel->setStyleSheet(this->getAsteropeCameraLabelStyle());
    ui->cameraApertureLabel->setStyleSheet(this->getAsteropeCameraLabelStyle());
    ui->cameraConnectLabel->setStyleSheet(this->getAsteropeCameraLabelStyle());
    ui->cameraNextLabel->setStyleSheet(this->getAsteropeCameraCtrlLabelStyle());

    ui->cameraCaptureLabel->setText(tr("CAPTURE"));
    ui->cameraExposureLabel->setText(tr("EXPOSURE TIME"));
    ui->cameraIsoLabel->setText(tr("ISO"));
    ui->cameraApertureLabel->setText(tr("APERTURE"));
    ui->cameraConnectLabel->setText(tr("CONNECT"));
    ui->cameraNextLabel->setText(tr("MORE"));

    QFont _font = ui->cameraCaptureLabel->font();
    _font.setPixelSize(_f);

    ui->cameraCaptureLabel->setFont(_font);
    ui->cameraExposureLabel->setFont(_font);
    ui->cameraIsoLabel->setFont(_font);
    ui->cameraApertureLabel->setFont(_font);
    ui->cameraConnectLabel->setFont(_font);
    ui->cameraNextLabel->setFont(_font);

    ui->asteropeCameraWidget->setStyleSheet(this->getAsteropeCameraWidgetStyle());
    ui->cameraCaptureButton->setStyleSheet(this->getAsteropeCameraButtonStyle());
    ui->cameraExposureButton->setStyleSheet(this->getAsteropeCameraButtonStyle());
    ui->cameraIsoButton->setStyleSheet(this->getAsteropeCameraButtonStyle());
    ui->cameraApertureButton->setStyleSheet(this->getAsteropeCameraButtonStyle());
    ui->cameraConnectButton->setStyleSheet(this->getAsteropeCameraButtonStyle());
    ui->cameraNextButton->setStyleSheet(this->getAsteropeCameraCtrlButtonStyle());
}

void AsteropeCameraWidget::on_cameraNextButton_pressed()
{
    qDebug() << "Camera | Next button pressed";
    emit asteropeCameraMenuButton_pressed(CAMERA_SECOND_PAGE);
}
