#include "../../include/about/asteropeaboutuswidget.h"
#include "ui_asteropeaboutuswidget.h"

AsteropeAboutUsWidget::AsteropeAboutUsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AsteropeAboutUsWidget)
{
    ui->setupUi(this);
    initAsteropeAboutUsMainWidget();
}

AsteropeAboutUsWidget::~AsteropeAboutUsWidget()
{
    delete ui;
}

void AsteropeAboutUsWidget::resizeEvent(QResizeEvent *)
{
    initAsteropeAboutUsMainWidget();
}

void AsteropeAboutUsWidget::showEvent(QShowEvent *)
{
    initAsteropeAboutUsMainWidget();
}

void AsteropeAboutUsWidget::initAsteropeAboutUsMainWidget()
{
    QSize _buttonSize;
    _buttonSize = ui->aboutUsAsteropeButton->size();
    int _w = static_cast<int>(_buttonSize.width()*0.8);
    int _h = static_cast<int>(_buttonSize.height()*0.8);
    int _f = static_cast<int>(_buttonSize.height()*0.1);
    QSize _size = QSize(_w,_h);
    ui->aboutUsAsteropeButton->setIconSize(_size);
    ui->aboutUsQtButton->setIconSize(_size);
    ui->aboutUsCosmoppButton->setIconSize(_size);
    ui->aboutUsArteskyButton->setIconSize(_size);
    ui->aboutUsGphotoButton->setIconSize(_size);
    ui->aboutUsPHDButton->setIconSize(_size);

    ui->aboutUsAsteropeLabel->setStyleSheet(this->getAsteropeAboutUsLabelStyle());
    ui->aboutUsQtLabel->setStyleSheet(this->getAsteropeAboutUsLabelStyle());
    ui->aboutUsCosmoppLabel->setStyleSheet(this->getAsteropeAboutUsLabelStyle());
    ui->aboutUsArteskyLabel->setStyleSheet(this->getAsteropeAboutUsLabelStyle());
    ui->aboutUsGphotoLabel->setStyleSheet(this->getAsteropeAboutUsLabelStyle());
    ui->aboutUsPHDLabel->setStyleSheet(this->getAsteropeAboutUsLabelStyle());

    ui->aboutUsAsteropeLabel->setText(tr("ASTEROPE"));
    ui->aboutUsQtLabel->setText(tr("Qt"));
    ui->aboutUsCosmoppLabel->setText(tr("COSMO++"));
    ui->aboutUsArteskyLabel->setText(tr("LIBQARTESKY"));
    ui->aboutUsGphotoLabel->setText(tr("LIBGPHOTO2"));
    ui->aboutUsPHDLabel->setText(tr("PHD2"));

    QFont _font = ui->aboutUsAsteropeLabel->font();
    _font.setPixelSize(_f);

    ui->aboutUsAsteropeLabel->setFont(_font);
    ui->aboutUsQtLabel->setFont(_font);
    ui->aboutUsCosmoppLabel->setFont(_font);
    ui->aboutUsArteskyLabel->setFont(_font);
    ui->aboutUsGphotoLabel->setFont(_font);
    ui->aboutUsPHDLabel->setFont(_font);

    ui->asteropeAboutUsWidget->setStyleSheet(this->getAsteropeAboutUsWidgetStyle());
    ui->aboutUsAsteropeButton->setStyleSheet(this->getAsteropeAboutUsButtonStyle());
    ui->aboutUsQtButton->setStyleSheet(this->getAsteropeAboutUsButtonStyle());
    ui->aboutUsCosmoppButton->setStyleSheet(this->getAsteropeAboutUsButtonStyle());
    ui->aboutUsArteskyButton->setStyleSheet(this->getAsteropeAboutUsButtonStyle());
    ui->aboutUsGphotoButton->setStyleSheet(this->getAsteropeAboutUsButtonStyle());
    ui->aboutUsPHDButton->setStyleSheet(this->getAsteropeAboutUsButtonStyle());
}
