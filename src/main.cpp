#include "../include/asterope.h"

#include <QApplication>
#include <QDesktopWidget>
//#include <QSplashScreen> //STATIC SPLASH
#include <QTimer> //SPLASH
#include <QDebug>
#include <QLabel> //DYNAMIC SPLASH
#include <QMovie> //DYNAMIC SPLASH
#include <QStyle> //DYNAMIC SPLASH
#include <QScreen>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    //QPixmap pixmap(":/images/splash.jpg"); //STATIC SPLASH
    //QSplashScreen splash(pixmap, Qt::WindowStaysOnTopHint ); //STATIC SPLASH
    Asterope w;
    QDesktopWidget dw;
    int ret = 0;
    /*******************************************************/
    //DYNAMIC SPLASH
    QMovie *movie = new QMovie(":/images/splash.gif");
    QLabel *processLabel = new QLabel(nullptr);
    processLabel->resize(480,320);  // to make sure its large enough
    processLabel->setMovie(movie);
    processLabel->setWindowFlags(Qt::FramelessWindowHint);
    processLabel->setAlignment(Qt::AlignCenter);
//    processLabel->setGeometry(QStyle::alignedRect(Qt::LeftToRight,Qt::AlignCenter,processLabel->size(),qApp->desktop()->availableGeometry())); //DEPRECATED
    processLabel->setGeometry(QStyle::alignedRect(Qt::LeftToRight,Qt::AlignCenter,processLabel->size(),qApp->primaryScreen()->availableGeometry()));

    movie->start();
    processLabel->show();
    /******************************************************/

    qApp->setApplicationVersion(APP_VERSION);
    qApp->setApplicationName(QString(APP_NAME));
    qApp->setOrganizationName("ASTROtrezzi");
    qApp->setOrganizationDomain("http://www.astrotrezzi.it");

    w.setWindowTitle(qApp->applicationName() + QString(" | Debug Version | v.") + qApp->applicationVersion()); //DEBUG
    w.setMinimumSize(QSize(480,320));
    w.setMaximumSize(QSize(1152,768));
    w.setWindowIcon(QIcon(":/icons/common/asterope.png"));
    if((dw.width() > w.maximumWidth()) || (dw.height() > w.maximumHeight()))
    {
          //set Maximum to size
          //w.setFixedSize(w.maximumSize());
    }
    else
    {
          //set to full screen
          w.setWindowFlags( Qt::WindowStaysOnTopHint | Qt::CustomizeWindowHint | Qt::FramelessWindowHint );
          w.setFixedSize(dw.size());
    }

    if((dw.width() < w.minimumWidth()) || (dw.height() < w.minimumHeight()))
    {
        //screen resolution  error managment
        //create a resolution error popup showed in full screen
        ret = -1;
    }
    else
    {
        //splash.show(); //STATIC SPLASH
        //QTimer::singleShot(3000, &splash, &QWidget::close); //STATIC SPLASH
        //QTimer::singleShot(3500, &w, &QWidget::show); //STATIC SPLASH

        QTimer::singleShot(3500,processLabel,SLOT(close())); //DYNAMIC SPLASH
        QTimer::singleShot(3500,&w,SLOT(show())); //DYNAMIC SPLASH

//        w.show(); //DEBUG: NO SPLASH
        ret = a.exec();
    }
    return ret;
}
