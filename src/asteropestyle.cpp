#include "../include/asteropestyle.h"

QString AsteropeStyle::getSettingsWidgetStyle(const AsteropeSection section)
{
    QString _style = "";
    switch(section)
    {
    case ASTEROPE_SETTINGS:
    case ASTEROPE_ABOUTUS:
    case ASTEROPE_SEQUENCE:
    case ASTEROPE_ASSISTANTS:
    case ASTEROPE_CAMERA:
    case ASTEROPE_CTRL:
        _style = "QWidget{"
                 "background-color: rgb(53, 47, 55);}";
        break;
    case ASTEROPE_EXIT:
        break;
    }
    return _style;
}

QString AsteropeStyle::getSettingsButtonStyle(const AsteropeSection section)
{
    QString _style = "";
    switch(section)
    {
    case ASTEROPE_SETTINGS:
        _style = "QToolButton {"
                "border: 1px solid black;"
                "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #443239, stop: 1 #35272d);"
                "min-width: 80px;}"

                "QToolButton:pressed {"
                    "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #35272d, stop: 1 #443239);}"

                "QToolButton:flat {"
                    "border: none;}"

                "QToolButton:default {"
                    "border-color: black;}";
        break;
    case ASTEROPE_CAMERA:
        _style = "QToolButton {"
                "border: 1px solid black;"
                "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #363f3d, stop: 1 #2c3331);"
                "min-width: 80px;}"

                "QToolButton:pressed {"
                    "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #2c3331, stop: 1 #363f3d);}"

                "QToolButton:flat {"
                    "border: none;}"

                "QToolButton:default {"
                    "border-color: black;}";
        break;
    case ASTEROPE_ASSISTANTS:
        _style = "QToolButton {"
                "border: 1px solid black;"
                "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #473e32, stop: 1 #383128);"
                "min-width: 80px;}"

                "QToolButton:pressed {"
                    "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #383128, stop: 1 #473e32);}"

                "QToolButton:flat {"
                    "border: none;}"

                "QToolButton:default {"
                    "border-color: black;}";
        break;
    case ASTEROPE_SEQUENCE:
        _style = "QToolButton {"
                "border: 1px solid black;"
                "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #333948, stop: 1 #2c313e);"
                "min-width: 80px;}"

                "QToolButton:pressed {"
                    "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #2c313e, stop: 1 #333948);}"

                "QToolButton:flat {"
                    "border: none;}"

                "QToolButton:default {"
                    "border-color: black;}";
        break;
    case ASTEROPE_ABOUTUS:
        _style = "QToolButton {"
                "border: 1px solid black;"
                "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #493635, stop: 1 #3d2d2c);"
                "min-width: 80px;}"

                "QToolButton:pressed {"
                    "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #3d2d2c, stop: 1 #493635);}"

                "QToolButton:flat {"
                    "border: none;}"

                "QToolButton:default {"
                    "border-color: black;}";
        break;
    case ASTEROPE_EXIT:
        _style = "QToolButton {"
                "border: 1px solid black;"
                "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #3d3142, stop: 1 #362b3a);"
                "min-width: 80px;}"

                "QToolButton:pressed {"
                    "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #362b3a, stop: 1 #3d3142);}"

                "QToolButton:flat {"
                    "border: none;}"

                "QToolButton:default {"
                    "border-color: black;}";
        break;
    case ASTEROPE_CTRL:
        _style = "QToolButton {"
                "border: 1px solid black;"
                "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #0d0d0d, stop: 1 black);"
                "min-width: 80px;}"

                "QToolButton:pressed {"
                    "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 black, stop: 1 #0d0d0d);}"

                "QToolButton:flat {"
                    "border: none;}"

                "QToolButton:default {"
                    "border-color: black;}";
        break;
    }
    return _style;
}

QString AsteropeStyle::getSettingsLabelStyle(const AsteropeSection section)
{
    QString _style = "";
    switch(section)
    {
    case ASTEROPE_SETTINGS:
        _style = "QLabel {"
                "border: 1px solid black;"
                "padding: 2px;"
                "color: rgb(0, 0, 0);"
                "background-color: #d14a50;}";
        break;
    case ASTEROPE_CAMERA:
        _style = "QLabel {"
                "border: 1px solid black;"
                "padding: 2px;"
                "color: rgb(0, 0, 0);"
                "background-color: #3bd472;}";
        break;
    case ASTEROPE_ASSISTANTS:
        _style = "QLabel {"
                "border: 1px solid black;"
                "padding: 2px;"
                "color: rgb(0, 0, 0);"
                "background-color: #eec608;}";
        break;
    case ASTEROPE_SEQUENCE:
        break;
    case ASTEROPE_ABOUTUS:
        _style = "QLabel {"
                "border: 1px solid black;"
                "padding: 2px;"
                "color: rgb(0, 0, 0);"
                "background-color: #fd7822;}";
        break;
    case ASTEROPE_EXIT:
        break;
    case ASTEROPE_CTRL:
        _style = "QLabel {"
                "border: 1px solid black;"
                "padding: 2px;"
                "color: rgb(255, 255, 255);"
                "background-color: rgb(0, 0, 0);}";
        break;
    }
    return _style;
}
