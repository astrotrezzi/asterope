#include "../../include/settings/settingssequence.h"
#include "ui_settingssequence.h"

SettingsSequence::SettingsSequence(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsSequence)
{
    ui->setupUi(this);
    initSettingsSequenceMainWidget();
}

SettingsSequence::~SettingsSequence()
{
    delete ui;
}

void SettingsSequence::resizeEvent(QResizeEvent *)
{
    initSettingsSequenceMainWidget();
}

void SettingsSequence::showEvent(QShowEvent *)
{
    initSettingsSequenceMainWidget();
}

void SettingsSequence::initSettingsSequenceMainWidget()
{
    QSize _buttonSize;
    _buttonSize = ui->sequenceIndexButton->size();
    int _w = static_cast<int>(_buttonSize.width()*0.8);
    int _h = static_cast<int>(_buttonSize.height()*0.8);
    int _f = static_cast<int>(_buttonSize.height()*0.1);
    QSize _size = QSize(_w,_h);
    ui->sequenceIndexButton->setIconSize(_size);
    ui->sequenceFrameMaxNumButton->setIconSize(_size);
    ui->sequenceFrameTypeButton->setIconSize(_size);
    ui->sequenceSetDefaultsButton->setIconSize(_size);

    ui->sequenceIndexLabel->setStyleSheet(this->getAsteropeSettingsSequenceLabelStyle());
    ui->sequenceFrameMaxNumLabel->setStyleSheet(this->getAsteropeSettingsSequenceLabelStyle());
    ui->sequenceFrameTypeLabel->setStyleSheet(this->getAsteropeSettingsSequenceLabelStyle());
    ui->sequenceSetDefaultsLabel->setStyleSheet(this->getAsteropeSettingsSequenceLabelStyle());

    ui->sequenceIndexLabel->setText(tr("INDEX"));
    ui->sequenceFrameMaxNumLabel->setText(tr("MAX NUMBER"));
    ui->sequenceFrameTypeLabel->setText(tr("FRAME TYPE"));
    ui->sequenceSetDefaultsLabel->setText(tr("SET DEFAULTS"));

    QFont _font = ui->sequenceIndexLabel->font();
    _font.setPixelSize(_f);

    ui->sequenceIndexLabel->setFont(_font);
    ui->sequenceFrameMaxNumLabel->setFont(_font);
    ui->sequenceFrameTypeLabel->setFont(_font);
    ui->sequenceSetDefaultsLabel->setFont(_font);

    ui->settingsSequenceWidget->setStyleSheet(this->getAsteropeSettingsSequenceWidgetStyle());
    ui->sequenceIndexButton->setStyleSheet(this->getAsteropeSettingsSequenceButtonStyle());
    ui->sequenceFrameMaxNumButton->setStyleSheet(this->getAsteropeSettingsSequenceButtonStyle());
    ui->sequenceFrameTypeButton->setStyleSheet(this->getAsteropeSettingsSequenceButtonStyle());
    ui->sequenceSetDefaultsButton->setStyleSheet(this->getAsteropeSettingsSequenceButtonStyle());
}
