#include "../../include/settings/asteropesettingswidget.h"
#include "ui_asteropesettingswidget.h"
#include <QDebug>

AsteropeSettingsWidget::AsteropeSettingsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AsteropeSettingsWidget)
{
    ui->setupUi(this);
    initAsteropeSettingsMainWidget();
}

AsteropeSettingsWidget::~AsteropeSettingsWidget()
{
    delete ui;
}

void AsteropeSettingsWidget::resizeEvent(QResizeEvent *)
{
    initAsteropeSettingsMainWidget();
}

void AsteropeSettingsWidget::showEvent(QShowEvent *)
{
    initAsteropeSettingsMainWidget();
}

void AsteropeSettingsWidget::initAsteropeSettingsMainWidget()
{
    QSize _buttonSize;
    _buttonSize = ui->settingsLanguageButton->size();
    int _w = static_cast<int>(_buttonSize.width()*0.8);
    int _h = static_cast<int>(_buttonSize.height()*0.8);
    int _f = static_cast<int>(_buttonSize.height()*0.1);
    QSize _size = QSize(_w,_h);
    ui->settingsLanguageButton->setIconSize(_size);
    ui->settingsConfigureButton->setIconSize(_size);
    ui->settingsSequenceButton->setIconSize(_size);
    ui->settingsUsbButton->setIconSize(_size);
    ui->settingsSetupButton->setIconSize(_size);
    ui->settingsDiagnosticsButton->setIconSize(_size);

    ui->settingsLanguageLabel->setStyleSheet(this->getAsteropeSettingsLabelStyle());
    ui->settingsConfigureLabel->setStyleSheet(this->getAsteropeSettingsLabelStyle());
    ui->settingsSequenceLabel->setStyleSheet(this->getAsteropeSettingsLabelStyle());
    ui->settingsUsbLabel->setStyleSheet(this->getAsteropeSettingsLabelStyle());
    ui->settingsSetupLabel->setStyleSheet(this->getAsteropeSettingsLabelStyle());
    ui->settingsDiagnosticsLabel->setStyleSheet(this->getAsteropeSettingsLabelStyle());

    ui->settingsLanguageLabel->setText(tr("LANGUAGE"));
    ui->settingsConfigureLabel->setText(tr("CONFIGURE"));
    ui->settingsSequenceLabel->setText(tr("SEQUENCE"));
    ui->settingsUsbLabel->setText(tr("USBKEY"));
    ui->settingsSetupLabel->setText(tr("SETUP"));
    ui->settingsDiagnosticsLabel->setText(tr("DIAGNOSTICS"));

    QFont _font = ui->settingsLanguageLabel->font();
    _font.setPixelSize(_f);

    ui->settingsLanguageLabel->setFont(_font);
    ui->settingsConfigureLabel->setFont(_font);
    ui->settingsSequenceLabel->setFont(_font);
    ui->settingsUsbLabel->setFont(_font);
    ui->settingsSetupLabel->setFont(_font);
    ui->settingsDiagnosticsLabel->setFont(_font);

    ui->asteropeSettingsWidget->setStyleSheet(this->getAsteropeSettingsWidgetStyle());
    ui->settingsLanguageButton->setStyleSheet(this->getAsteropeSettingsButtonStyle());
    ui->settingsConfigureButton->setStyleSheet(this->getAsteropeSettingsButtonStyle());
    ui->settingsSequenceButton->setStyleSheet(this->getAsteropeSettingsButtonStyle());
    ui->settingsUsbButton->setStyleSheet(this->getAsteropeSettingsButtonStyle());
    ui->settingsSetupButton->setStyleSheet(this->getAsteropeSettingsButtonStyle());
    ui->settingsDiagnosticsButton->setStyleSheet(this->getAsteropeSettingsButtonStyle());
}

void AsteropeSettingsWidget::on_settingsLanguageButton_pressed()
{
    qDebug() << "Settings | Language button pressed";
    emit asteropeSettingsMenuButton_pressed(SETTINGS_LANGUAGE);
}

void AsteropeSettingsWidget::on_settingsConfigureButton_pressed()
{
    qDebug() << "Settings | Configure button pressed";
    emit asteropeSettingsMenuButton_pressed(SETTINGS_CONFIGURE);
}

void AsteropeSettingsWidget::on_settingsSequenceButton_pressed()
{
    qDebug() << "Settings | Sequence button pressed";
    emit asteropeSettingsMenuButton_pressed(SETTINGS_SEQUENCE);
}

void AsteropeSettingsWidget::on_settingsUsbButton_pressed()
{
    qDebug() << "Settings | UsbKey button pressed";
    emit asteropeSettingsMenuButton_pressed(SETTINGS_USB);
}

void AsteropeSettingsWidget::on_settingsSetupButton_pressed()
{
    qDebug() << "Settings | Setup button pressed";
    emit asteropeSettingsMenuButton_pressed(SETTINGS_SETUP);
}

void AsteropeSettingsWidget::on_settingsDiagnosticsButton_pressed()
{
    qDebug() << "Settings | Diagnostics button pressed";
    emit asteropeSettingsMenuButton_pressed(SETTINGS_DIAGNOSTICS);
}
