#include "../../include/settings/settingslanguage.h"
#include "ui_settingslanguage.h"

SettingsLanguage::SettingsLanguage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsLanguage)
{
    ui->setupUi(this);
    initSettingsLanguageMainWidget();
}

SettingsLanguage::~SettingsLanguage()
{
    delete ui;
}

void SettingsLanguage::resizeEvent(QResizeEvent *)
{
    initSettingsLanguageMainWidget();
}

void SettingsLanguage::showEvent(QShowEvent *)
{
    initSettingsLanguageMainWidget();
}

void SettingsLanguage::initSettingsLanguageMainWidget()
{
    QSize _buttonSize;
    _buttonSize = ui->languageUKButton->size();
    int _w = static_cast<int>(_buttonSize.width()*0.8);
    int _h = static_cast<int>(_buttonSize.height()*0.8);
    int _f = static_cast<int>(_buttonSize.height()*0.1);
    QSize _size = QSize(_w,_h);
    ui->languageUKButton->setIconSize(_size);
    ui->languageITButton->setIconSize(_size);
    ui->languageFRButton->setIconSize(_size);
    ui->languageESButton->setIconSize(_size);
    ui->languageDEButton->setIconSize(_size);
    ui->languageCNButton->setIconSize(_size);

    ui->languageUKLabel->setStyleSheet(this->getAsteropeLanguageLabelStyle());
    ui->languageITLabel->setStyleSheet(this->getAsteropeLanguageLabelStyle());
    ui->languageFRLabel->setStyleSheet(this->getAsteropeLanguageLabelStyle());
    ui->languageESLabel->setStyleSheet(this->getAsteropeLanguageLabelStyle());
    ui->languageDELabel->setStyleSheet(this->getAsteropeLanguageLabelStyle());
    ui->languageCNLabel->setStyleSheet(this->getAsteropeLanguageLabelStyle());

    ui->languageUKLabel->setText(tr("ENGLISH"));
    ui->languageITLabel->setText(tr("ITALIAN"));
    ui->languageFRLabel->setText(tr("FRENCH"));
    ui->languageESLabel->setText(tr("SPANISH"));
    ui->languageDELabel->setText(tr("GERMAN"));
    ui->languageCNLabel->setText(tr("CHINESE"));

    QFont _font = ui->languageUKLabel->font();
    _font.setPixelSize(_f);

    ui->languageUKLabel->setFont(_font);
    ui->languageITLabel->setFont(_font);
    ui->languageFRLabel->setFont(_font);
    ui->languageESLabel->setFont(_font);
    ui->languageDELabel->setFont(_font);
    ui->languageCNLabel->setFont(_font);

    ui->settingsLanguageWidget->setStyleSheet(this->getAsteropeLanguageWidgetStyle());
    ui->languageUKButton->setStyleSheet(this->getAsteropeLanguageButtonStyle());
    ui->languageITButton->setStyleSheet(this->getAsteropeLanguageButtonStyle());
    ui->languageFRButton->setStyleSheet(this->getAsteropeLanguageButtonStyle());
    ui->languageESButton->setStyleSheet(this->getAsteropeLanguageButtonStyle());
    ui->languageDEButton->setStyleSheet(this->getAsteropeLanguageButtonStyle());
    ui->languageCNButton->setStyleSheet(this->getAsteropeLanguageButtonStyle());
}
