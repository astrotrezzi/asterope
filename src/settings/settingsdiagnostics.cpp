#include "../../include/settings/settingsdiagnostics.h"
#include "ui_settingsdiagnostics.h"

SettingsDiagnostics::SettingsDiagnostics(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsDiagnostics)
{
    ui->setupUi(this);
    initSettingsDiagnosticsMainWidget();
}

SettingsDiagnostics::~SettingsDiagnostics()
{
    delete ui;
}

void SettingsDiagnostics::resizeEvent(QResizeEvent *)
{
    initSettingsDiagnosticsMainWidget();
}

void SettingsDiagnostics::showEvent(QShowEvent *)
{
    initSettingsDiagnosticsMainWidget();
}

void SettingsDiagnostics::initSettingsDiagnosticsMainWidget()
{
    QSize _buttonSize;
    _buttonSize = ui->diagnosticsDevicesButton->size();
    int _w = static_cast<int>(_buttonSize.width()*0.8);
    int _h = static_cast<int>(_buttonSize.height()*0.8);
    int _f = static_cast<int>(_buttonSize.height()*0.1);
    QSize _size = QSize(_w,_h);
    ui->diagnosticsDevicesButton->setIconSize(_size);
    ui->diagnosticsLogButton->setIconSize(_size);
    ui->diagnosticsErrorListButton->setIconSize(_size);
    ui->diagnosticsSystemCheckButton->setIconSize(_size);

    ui->diagnosticsDevicesLabel->setStyleSheet(this->getAsteropeSettingsDiagnosticsLabelStyle());
    ui->diagnosticsLogLabel->setStyleSheet(this->getAsteropeSettingsDiagnosticsLabelStyle());
    ui->diagnosticsErrorListLabel->setStyleSheet(this->getAsteropeSettingsDiagnosticsLabelStyle());
    ui->diagnosticsSystemCheckLabel->setStyleSheet(this->getAsteropeSettingsDiagnosticsLabelStyle());

    ui->diagnosticsDevicesLabel->setText(tr("DEVICES"));
    ui->diagnosticsLogLabel->setText(tr("ENABLE LOG"));
    ui->diagnosticsErrorListLabel->setText(tr("ERROR LIST"));
    ui->diagnosticsSystemCheckLabel->setText(tr("SYSTEM CHECK"));

    QFont _font = ui->diagnosticsDevicesLabel->font();
    _font.setPixelSize(_f);

    ui->diagnosticsDevicesLabel->setFont(_font);
    ui->diagnosticsLogLabel->setFont(_font);
    ui->diagnosticsErrorListLabel->setFont(_font);
    ui->diagnosticsSystemCheckLabel->setFont(_font);

    ui->settingsDiagnosticsWidget->setStyleSheet(this->getAsteropeSettingsDiagnosticsWidgetStyle());
    ui->diagnosticsDevicesButton->setStyleSheet(this->getAsteropeSettingsDiagnosticsButtonStyle());
    ui->diagnosticsLogButton->setStyleSheet(this->getAsteropeSettingsDiagnosticsButtonStyle());
    ui->diagnosticsErrorListButton->setStyleSheet(this->getAsteropeSettingsDiagnosticsButtonStyle());
    ui->diagnosticsSystemCheckButton->setStyleSheet(this->getAsteropeSettingsDiagnosticsButtonStyle());
}
