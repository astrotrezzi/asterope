#include "../../include/settings/settingsconfigure.h"
#include "ui_settingsconfigure.h"

SettingsConfigure::SettingsConfigure(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsConfigure)
{
    ui->setupUi(this);
    initSettingsConfigureMainWidget();
}

SettingsConfigure::~SettingsConfigure()
{
    delete ui;
}

void SettingsConfigure::resizeEvent(QResizeEvent *)
{
    initSettingsConfigureMainWidget();
}

void SettingsConfigure::showEvent(QShowEvent *)
{
    initSettingsConfigureMainWidget();
}

void SettingsConfigure::initSettingsConfigureMainWidget()
{
    QSize _buttonSize;
    _buttonSize = ui->configureRootButton->size();
    int _w = static_cast<int>(_buttonSize.width()*0.8);
    int _h = static_cast<int>(_buttonSize.height()*0.8);
    int _f = static_cast<int>(_buttonSize.height()*0.1);
    QSize _size = QSize(_w,_h);
    ui->configureRootButton->setIconSize(_size);
    ui->configureImageDirButton->setIconSize(_size);
    ui->configureReportDirButton->setIconSize(_size);
    ui->configureLogDirButton->setIconSize(_size);
    ui->configureSetDefaultsButton->setIconSize(_size);

    ui->configureRootLabel->setStyleSheet(this->getAsteropeConfigureLabelStyle());
    ui->configureImageDirLabel->setStyleSheet(this->getAsteropeConfigureLabelStyle());
    ui->configureReportDirLabel->setStyleSheet(this->getAsteropeConfigureLabelStyle());
    ui->configureLogDirLabel->setStyleSheet(this->getAsteropeConfigureLabelStyle());
    ui->configureSetDefaultsLabel->setStyleSheet(this->getAsteropeConfigureLabelStyle());

    ui->configureRootLabel->setText(tr("ROOT DIRECTORY"));
    ui->configureImageDirLabel->setText(tr("IMAGES FOLDER"));
    ui->configureReportDirLabel->setText(tr("REPORT FOLDER"));
    ui->configureLogDirLabel->setText(tr("LOG FOLDER"));
    ui->configureSetDefaultsLabel->setText("SET DEFAULTS");

    QFont _font = ui->configureRootLabel->font();
    _font.setPixelSize(_f);

    ui->configureRootLabel->setFont(_font);
    ui->configureImageDirLabel->setFont(_font);
    ui->configureReportDirLabel->setFont(_font);
    ui->configureLogDirLabel->setFont(_font);
    ui->configureSetDefaultsLabel->setFont(_font);

    ui->settingsConfigureWidget->setStyleSheet(this->getAsteropeConfigureWidgetStyle());
    ui->configureRootButton->setStyleSheet(this->getAsteropeConfigureButtonStyle());
    ui->configureImageDirButton->setStyleSheet(this->getAsteropeConfigureButtonStyle());
    ui->configureReportDirButton->setStyleSheet(this->getAsteropeConfigureButtonStyle());
    ui->configureLogDirButton->setStyleSheet(this->getAsteropeConfigureButtonStyle());
    ui->configureSetDefaultsButton->setStyleSheet(this->getAsteropeConfigureButtonStyle());
}
