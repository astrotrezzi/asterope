#include "../../include/settings/settingsusbkey.h"
#include "ui_settingsusbkey.h"

SettingsUsbKey::SettingsUsbKey(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsUsbKey)
{
    ui->setupUi(this);
    initSettingsUsbMainWidget();
}

SettingsUsbKey::~SettingsUsbKey()
{
    delete ui;
}

void SettingsUsbKey::resizeEvent(QResizeEvent *)
{
    initSettingsUsbMainWidget();
}

void SettingsUsbKey::showEvent(QShowEvent *)
{
    initSettingsUsbMainWidget();
}

void SettingsUsbKey::initSettingsUsbMainWidget()
{
    QSize _buttonSize;
    _buttonSize = ui->usbFileFormatButton->size();
    int _w = static_cast<int>(_buttonSize.width()*0.8);
    int _h = static_cast<int>(_buttonSize.height()*0.8);
    int _f = static_cast<int>(_buttonSize.height()*0.1);
    QSize _size = QSize(_w,_h);
    ui->usbFileFormatButton->setIconSize(_size);
    ui->usbImageDirButton->setIconSize(_size);
    ui->usbReportDirButton->setIconSize(_size);
    ui->usbLogDirButton->setIconSize(_size);
    ui->usbUmountButton->setIconSize(_size);
    ui->usbSetDefaultsButton->setIconSize(_size);

    ui->usbFileFormatLabel->setStyleSheet(this->getAsteropeUsbLabelStyle());
    ui->usbImageDirLabel->setStyleSheet(this->getAsteropeUsbLabelStyle());
    ui->usbReportDirLabel->setStyleSheet(this->getAsteropeUsbLabelStyle());
    ui->usbLogDirLabel->setStyleSheet(this->getAsteropeUsbLabelStyle());
    ui->usbUmountLabel->setStyleSheet(this->getAsteropeUsbLabelStyle());
    ui->usbSetDefaultsLabel->setStyleSheet(this->getAsteropeUsbLabelStyle());

    ui->usbFileFormatLabel->setText(tr("FILE FORMAT"));
    ui->usbImageDirLabel->setText(tr("IMAGES FOLDER"));
    ui->usbReportDirLabel->setText(tr("REPORT FOLDER"));
    ui->usbUmountLabel->setText(tr("REMOVE USB KEY"));
    ui->usbLogDirLabel->setText(tr("LOG FOLDER"));
    ui->usbSetDefaultsLabel->setText("SET DEFAULTS");

    QFont _font = ui->usbFileFormatLabel->font();
    _font.setPixelSize(_f);

    ui->usbFileFormatLabel->setFont(_font);
    ui->usbImageDirLabel->setFont(_font);
    ui->usbReportDirLabel->setFont(_font);
    ui->usbUmountLabel->setFont(_font);
    ui->usbLogDirLabel->setFont(_font);
    ui->usbSetDefaultsLabel->setFont(_font);

    ui->settingsUsbWidget->setStyleSheet(this->getAsteropeUsbWidgetStyle());
    ui->usbFileFormatButton->setStyleSheet(this->getAsteropeUsbButtonStyle());
    ui->usbImageDirButton->setStyleSheet(this->getAsteropeUsbButtonStyle());
    ui->usbReportDirButton->setStyleSheet(this->getAsteropeUsbButtonStyle());
    ui->usbLogDirButton->setStyleSheet(this->getAsteropeUsbButtonStyle());
    ui->usbUmountButton->setStyleSheet(this->getAsteropeUsbButtonStyle());
    ui->usbSetDefaultsButton->setStyleSheet(this->getAsteropeUsbButtonStyle());
}
