#include "../../include/settings/settingssetup.h"
#include "ui_settingssetup.h"

SettingsSetup::SettingsSetup(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsSetup)
{
    ui->setupUi(this);
    initSettingsSetupMainWidget();
}

SettingsSetup::~SettingsSetup()
{
    delete ui;
}

void SettingsSetup::resizeEvent(QResizeEvent *)
{
    initSettingsSetupMainWidget();
}

void SettingsSetup::showEvent(QShowEvent *)
{
    initSettingsSetupMainWidget();
}

void SettingsSetup::initSettingsSetupMainWidget()
{
    QSize _buttonSize;
    _buttonSize = ui->setupTelescopeButton->size();
    int _w = static_cast<int>(_buttonSize.width()*0.8);
    int _h = static_cast<int>(_buttonSize.height()*0.8);
    int _f = static_cast<int>(_buttonSize.height()*0.1);
    QSize _size = QSize(_w,_h);
    ui->setupTelescopeButton->setIconSize(_size);
    ui->setupCameraButton->setIconSize(_size);
    ui->setupFiltersButton->setIconSize(_size);
    ui->setupGuideButton->setIconSize(_size);
    ui->setupObjectButton->setIconSize(_size);
    ui->setupSetDefaultsButton->setIconSize(_size);

    ui->setupTelescopeLabel->setStyleSheet(this->getAsteropeSetupLabelStyle());
    ui->setupCameraLabel->setStyleSheet(this->getAsteropeSetupLabelStyle());
    ui->setupFiltersLabel->setStyleSheet(this->getAsteropeSetupLabelStyle());
    ui->setupGuideLabel->setStyleSheet(this->getAsteropeSetupLabelStyle());
    ui->setupObjectLabel->setStyleSheet(this->getAsteropeSetupLabelStyle());
    ui->setupSetDefaultsLabel->setStyleSheet(this->getAsteropeSetupLabelStyle());

    ui->setupTelescopeLabel->setText(tr("TELESCOPE"));
    ui->setupCameraLabel->setText(tr("CAMERA"));
    ui->setupFiltersLabel->setText(tr("FILTERS"));
    ui->setupGuideLabel->setText(tr("AUTOGUIDE"));
    ui->setupObjectLabel->setText(tr("OBJCT"));
    ui->setupSetDefaultsLabel->setText("SET DEFAULTS");

    QFont _font = ui->setupTelescopeLabel->font();
    _font.setPixelSize(_f);

    ui->setupTelescopeLabel->setFont(_font);
    ui->setupCameraLabel->setFont(_font);
    ui->setupFiltersLabel->setFont(_font);
    ui->setupGuideLabel->setFont(_font);
    ui->setupObjectLabel->setFont(_font);
    ui->setupSetDefaultsLabel->setFont(_font);

    ui->settingsSetupWidget->setStyleSheet(this->getAsteropeSetupWidgetStyle());
    ui->setupTelescopeButton->setStyleSheet(this->getAsteropeSetupButtonStyle());
    ui->setupCameraButton->setStyleSheet(this->getAsteropeSetupButtonStyle());
    ui->setupFiltersButton->setStyleSheet(this->getAsteropeSetupButtonStyle());
    ui->setupGuideButton->setStyleSheet(this->getAsteropeSetupButtonStyle());
    ui->setupObjectButton->setStyleSheet(this->getAsteropeSetupButtonStyle());
    ui->setupSetDefaultsButton->setStyleSheet(this->getAsteropeSetupButtonStyle());
}
