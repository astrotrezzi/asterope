#include "../include/asterope.h"
#include "ui_asterope.h"
#include <QSize>
#include <QBitmap>
#include "../include/asteropeconst.h"
#include <QDebug>
#include <QDialog>

Asterope::Asterope(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Asterope)
{
    ui->setupUi(this);
    m_asteropeMainWidget = new AsteropeMainWidget();
    m_asteropeSettingsWidget = new AsteropeSettingsWidget();
    m_asteropeCameraWidget = new AsteropeCameraWidget();
    m_asteropeCameraWidget2 = new AsteropeCameraWidget2();
    m_asteropeAssistantsWidget = new AsteropeAssistantsWidget();
    m_asteropeAssistantsWidget2 = new AsteropeAssistantsWidget2();
    m_asteropeSequenceWidget = new AsteropeSequenceWidget();
    m_asteropeAboutUsWidget = new AsteropeAboutUsWidget();
    m_asteropeBlackWidget = new AsteropeBlackWidget(this);
    //settings
    m_asteropeSettingsLanguage = new SettingsLanguage();
    m_asteropeSettingsConfigure = new SettingsConfigure();
    m_asteropeSettingsSequence = new SettingsSequence();
    m_asteropeSettingsUsbKey = new SettingsUsbKey();
    m_asteropeSettingsSetup = new SettingsSetup();
    m_asteropeSettingsDiagnostics = new SettingsDiagnostics();

    connect(m_asteropeMainWidget, &AsteropeMainWidget::asteropeMainMenuButton_pressed,this, &Asterope::on_asteropeMainMenuButton_pressed);
    connect(m_asteropeSettingsWidget, &AsteropeSettingsWidget::asteropeSettingsMenuButton_pressed, this, &Asterope::on_asteropeSettingsMenuButton_pressed);
    connect(m_asteropeCameraWidget, &AsteropeCameraWidget::asteropeCameraMenuButton_pressed, this, &Asterope::on_asteropeCameraMenuButton_pressed);
    connect(m_asteropeCameraWidget2, &AsteropeCameraWidget2::asteropeCameraMenuButton_pressed, this, &Asterope::on_asteropeCameraMenuButton_pressed);
    connect(m_asteropeAssistantsWidget, &AsteropeAssistantsWidget::asteropeAssistantsMenuButton_pressed, this, &Asterope::on_asteropeAssistantsMenuButton_pressed);
    connect(m_asteropeAssistantsWidget2, &AsteropeAssistantsWidget2::asteropeAssistantsMenuButton_pressed, this, &Asterope::on_asteropeAssistantsMenuButton_pressed);
    this->initMenuPanel();
}

Asterope::~Asterope()
{
    disconnect(m_asteropeSettingsWidget, &AsteropeSettingsWidget::asteropeSettingsMenuButton_pressed, this, &Asterope::on_asteropeSettingsMenuButton_pressed);
    disconnect(m_asteropeMainWidget, &AsteropeMainWidget::asteropeMainMenuButton_pressed,this, &Asterope::on_asteropeMainMenuButton_pressed);
    disconnect(m_asteropeCameraWidget, &AsteropeCameraWidget::asteropeCameraMenuButton_pressed, this, &Asterope::on_asteropeCameraMenuButton_pressed);
    disconnect(m_asteropeCameraWidget2, &AsteropeCameraWidget2::asteropeCameraMenuButton_pressed, this, &Asterope::on_asteropeCameraMenuButton_pressed);
    disconnect(m_asteropeAssistantsWidget, &AsteropeAssistantsWidget::asteropeAssistantsMenuButton_pressed, this, &Asterope::on_asteropeAssistantsMenuButton_pressed);
    disconnect(m_asteropeAssistantsWidget2, &AsteropeAssistantsWidget2::asteropeAssistantsMenuButton_pressed, this, &Asterope::on_asteropeAssistantsMenuButton_pressed);

    delete m_asteropeMainWidget;
    delete m_asteropeSettingsWidget;
    delete m_asteropeCameraWidget;
    delete m_asteropeCameraWidget2;
    delete m_asteropeAssistantsWidget;
    delete m_asteropeAssistantsWidget2;
    delete m_asteropeSequenceWidget;
    delete m_asteropeAboutUsWidget;
    delete m_asteropeBlackWidget;
    //settings
    delete m_asteropeSettingsLanguage;
    delete m_asteropeSettingsConfigure;
    delete m_asteropeSettingsSequence;
    delete m_asteropeSettingsUsbKey;
    delete m_asteropeSettingsSetup;
    delete m_asteropeSettingsDiagnostics;

    delete ui;
}

void Asterope::resizeEvent(QResizeEvent * /* event */)
{
    this->initMainWindow();
    this->initCtrlPanel();
}

void Asterope::showEvent(QShowEvent *)
{
    this->initMainWindow();
    this->initCtrlPanel();
}

void Asterope::initMenuPanel()
{
    //initialize menu Panel
    ui->menuStackedWidget->addWidget(m_asteropeMainWidget);
    ui->menuStackedWidget->addWidget(m_asteropeSettingsWidget);
    ui->menuStackedWidget->addWidget(m_asteropeCameraWidget);
    ui->menuStackedWidget->addWidget(m_asteropeCameraWidget2);
    ui->menuStackedWidget->addWidget(m_asteropeAssistantsWidget);
    ui->menuStackedWidget->addWidget(m_asteropeAssistantsWidget2);
    ui->menuStackedWidget->addWidget(m_asteropeSequenceWidget);
    ui->menuStackedWidget->addWidget(m_asteropeAboutUsWidget);
    //settings
    ui->menuStackedWidget->addWidget(m_asteropeSettingsLanguage);
    ui->menuStackedWidget->addWidget(m_asteropeSettingsConfigure);
    ui->menuStackedWidget->addWidget(m_asteropeSettingsSequence);
    ui->menuStackedWidget->addWidget(m_asteropeSettingsUsbKey);
    ui->menuStackedWidget->addWidget(m_asteropeSettingsSetup);
    ui->menuStackedWidget->addWidget(m_asteropeSettingsDiagnostics);

    ui->menuStackedWidget->setCurrentWidget(m_asteropeMainWidget);
}

void Asterope::initCtrlPanel()
{
    //initialize control Layout (status and command)
    QPixmap _batteryIcon(BATTERY_DISCONNECT);
    QPixmap _connectIcon(DISCONNECT_ICON);
    QPixmap _backIcon(BACK_ICON);
    QPixmap _homeIcon(HOME_ICON);
    QPixmap _offIcon(OFF_ICON);
    ui->ctrlBatteryIcon->setPixmap(_batteryIcon.scaled(ui->ctrlBatteryIcon->width(),ui->ctrlBatteryIcon->height(),Qt::KeepAspectRatio));
    ui->ctrlConnectIcon->setPixmap(_connectIcon.scaled(ui->ctrlConnectIcon->width(),ui->ctrlConnectIcon->height(),Qt::KeepAspectRatio));
    ui->ctrlCmdListWidget->setIconSize(QSize(static_cast<int>(ui->ctrlCmdListWidget->width()*0.9),static_cast<int>(ui->ctrlCmdListWidget->width()*0.9)));
    int _size = static_cast<int>((ui->ctrlCmdListWidget->height()*0.33) + (ui->ctrlCmdListWidget->iconSize().height()*0.2));
    ui->ctrlCmdListWidget->setFocusPolicy(Qt::NoFocus);
    ui->ctrlCmdListWidget->setGridSize(QSize(ui->ctrlCmdListWidget->width(),_size));
    ui->ctrlStatusMainWidget->setStyleSheet(this->getCtrlStatusMainWidgetStyle());
    ui->ctrlCmdMainWidget->setStyleSheet(this->getCtrlCmdMainWidgetStyle());
}

void Asterope::initMainWindow()
{
    ui-> ctrlStatusMainWidget->setFixedSize(static_cast<int>(this->size().width()*0.15),static_cast<int>(this->size().height()*0.12));
    ui-> ctrlCmdMainWidget->setFixedSize(static_cast<int>(this->size().width()*0.15),static_cast<int>(this->size().height()*0.89));
}

void Asterope::on_ctrlCmdListWidget_itemPressed(QListWidgetItem *item)
{
    int _itemNum = 0;
    for(int i = 0; i < 3; i++)
    {
        if(ui->ctrlCmdListWidget->item(i) == item)
        {
            _itemNum = i;
            break;
        }
    }
    if(_itemNum == 1)
    {
        ui->menuStackedWidget->setCurrentWidget(m_asteropeMainWidget);
    }
    else if(_itemNum == 0)
    {
        m_currentWidgetIdx = m_previousWidgetIdx.at(m_previousWidgetIdx.size()-1);
        ui->menuStackedWidget->setCurrentIndex(m_currentWidgetIdx);
        if(m_previousWidgetIdx.size() > 1)
        {
            m_previousWidgetIdx.pop_back();
        }
        qDebug() << "previous widget Index (back):" << m_previousWidgetIdx.at(m_previousWidgetIdx.size()-1);
        qDebug() << "current  widget Index (back):" << m_currentWidgetIdx;
        qDebug() << "history size (back):" << m_previousWidgetIdx.size();
    }
    else
    {
        m_asteropeBlackWidget->setFixedSize(this->size().width(),this->size().height());
        m_asteropeBlackWidget->setWindowFlag(Qt::FramelessWindowHint);
        m_asteropeBlackWidget->show();
    }
}

void Asterope::on_asteropeMainMenuButton_pressed(AsteropeSection section)
{
    switch(section)
    {
    case ASTEROPE_SETTINGS:
        ui->menuStackedWidget->setCurrentWidget(m_asteropeSettingsWidget);
        break;
    case ASTEROPE_CAMERA:
        ui->menuStackedWidget->setCurrentWidget(m_asteropeCameraWidget);
        break;
    case ASTEROPE_ASSISTANTS:
        ui->menuStackedWidget->setCurrentWidget(m_asteropeAssistantsWidget);
        break;
    case ASTEROPE_SEQUENCE:
        ui->menuStackedWidget->setCurrentWidget(m_asteropeSequenceWidget);
        break;
    case ASTEROPE_ABOUTUS:
        ui->menuStackedWidget->setCurrentWidget(m_asteropeAboutUsWidget);
        break;
    case ASTEROPE_EXIT:
        QCoreApplication::exit(0);
        break;
    case ASTEROPE_CTRL:
        break;
    }
}


void Asterope::on_asteropeSettingsMenuButton_pressed(SettingsSection section)
{
    switch(section)
    {
    case SETTINGS_LANGUAGE:
        ui->menuStackedWidget->setCurrentWidget(m_asteropeSettingsLanguage);
        break;
    case SETTINGS_CONFIGURE:
        ui->menuStackedWidget->setCurrentWidget(m_asteropeSettingsConfigure);
        break;
    case SETTINGS_SEQUENCE:
        ui->menuStackedWidget->setCurrentWidget(m_asteropeSettingsSequence);
        break;
    case SETTINGS_USB:
        ui->menuStackedWidget->setCurrentWidget(m_asteropeSettingsUsbKey);
        break;
    case SETTINGS_SETUP:
        ui->menuStackedWidget->setCurrentWidget(m_asteropeSettingsSetup);
        break;
    case SETTINGS_DIAGNOSTICS:
        ui->menuStackedWidget->setCurrentWidget(m_asteropeSettingsDiagnostics);
        break;
    }
}

void Asterope::on_asteropeCameraMenuButton_pressed(CameraSection section)
{
    switch(section)
    {
    case CAMERA_FIRST_PAGE:
        ui->menuStackedWidget->setCurrentWidget(m_asteropeCameraWidget);
        break;
    case CAMERA_SECOND_PAGE:
        ui->menuStackedWidget->setCurrentWidget(m_asteropeCameraWidget2);
        break;
    }
}

void Asterope::on_asteropeAssistantsMenuButton_pressed(AssistantsSection section)
{
    switch(section)
    {
    case ASSISTANTS_FIRST_PAGE:
        ui->menuStackedWidget->setCurrentWidget(m_asteropeAssistantsWidget);
        break;
    case ASSISTANTS_SECOND_PAGE:
        ui->menuStackedWidget->setCurrentWidget(m_asteropeAssistantsWidget2);
        break;
    }
}

void Asterope::on_menuStackedWidget_currentChanged(int arg1)
{
    if(arg1 != m_currentWidgetIdx)
    {
        m_previousWidgetIdx.push_back(m_currentWidgetIdx);
        m_currentWidgetIdx = arg1;
        qDebug() << "previous widget Index:" << m_previousWidgetIdx.at(m_previousWidgetIdx.size()-1);
        qDebug() << "current  widget Index:" << m_currentWidgetIdx;
        qDebug() << "history size:" << m_previousWidgetIdx.size();
    }
}
