#include "../include/asteropemainwidget.h"
#include "ui_asteropemainwidget.h"
#include <QDebug>

AsteropeMainWidget::AsteropeMainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AsteropeMainWidget)
{
    ui->setupUi(this);
}

AsteropeMainWidget::~AsteropeMainWidget()
{
    delete ui;
}

void AsteropeMainWidget::resizeEvent(QResizeEvent *)
{
    qDebug() << "Resize Event emitted";
    initAsteropeMainWidget();
}

void AsteropeMainWidget::showEvent(QShowEvent *)
{
    qDebug() << "Show Event emitted";
    initAsteropeMainWidget();
}

void AsteropeMainWidget::initAsteropeMainWidget()
{
    QSize _buttonSize;
    _buttonSize = ui->asteropeCameraButton->size();
    qDebug() << "button size:" << _buttonSize;
    ui->asteropeSettingsButton->setIconSize(QSize(static_cast<int>(_buttonSize.width()*0.8),static_cast<int>(_buttonSize.height()*0.8)));
    ui->asteropeCameraButton->setIconSize(QSize(static_cast<int>(_buttonSize.width()*0.8),static_cast<int>(_buttonSize.height()*0.8)));
    ui->asteropeAssistantsButton->setIconSize(QSize(static_cast<int>(_buttonSize.width()*0.8),static_cast<int>(_buttonSize.height()*0.8)));
    ui->asteropeSequenceButton->setIconSize(QSize(static_cast<int>(_buttonSize.width()*0.8),static_cast<int>(_buttonSize.height()*0.8)));
    ui->asteropeAboutUsButton->setIconSize(QSize(static_cast<int>(_buttonSize.width()*0.8),static_cast<int>(_buttonSize.height()*0.8)));
    ui->asteropeExitButton->setIconSize(QSize(static_cast<int>(_buttonSize.width()*0.8),static_cast<int>(_buttonSize.height()*0.8)));
    ui->asteropeMainWidget->setStyleSheet(this->getAsteropeMainWidgetStyle());
    ui->asteropeSettingsButton->setStyleSheet(this->getAsteropeButtonStyle(ASTEROPE_SETTINGS));
    ui->asteropeCameraButton->setStyleSheet(this->getAsteropeButtonStyle(ASTEROPE_CAMERA));
    ui->asteropeAssistantsButton->setStyleSheet(this->getAsteropeButtonStyle(ASTEROPE_ASSISTANTS));
    ui->asteropeSequenceButton->setStyleSheet(this->getAsteropeButtonStyle(ASTEROPE_SEQUENCE));
    ui->asteropeAboutUsButton->setStyleSheet(this->getAsteropeButtonStyle(ASTEROPE_ABOUTUS));
    ui->asteropeExitButton->setStyleSheet(this->getAsteropeButtonStyle(ASTEROPE_EXIT));
    ui->asteropeSettingsLine->setStyleSheet(this->getAsteropeLineStyle(ASTEROPE_SETTINGS));
    ui->asteropeCameraLine->setStyleSheet(this->getAsteropeLineStyle(ASTEROPE_CAMERA));
    ui->asteropeAssistantsLine->setStyleSheet(this->getAsteropeLineStyle(ASTEROPE_ASSISTANTS));
    ui->asteropeSequenceLine->setStyleSheet(this->getAsteropeLineStyle(ASTEROPE_SEQUENCE));
    ui->asteropeAboutUsLine->setStyleSheet(this->getAsteropeLineStyle(ASTEROPE_ABOUTUS));
    ui->asteropeExitLine->setStyleSheet(this->getAsteropeLineStyle(ASTEROPE_EXIT));
}

void AsteropeMainWidget::on_asteropeSettingsButton_pressed()
{
    qDebug() << "Settings button pressed";
    emit asteropeMainMenuButton_pressed(ASTEROPE_SETTINGS);
}

void AsteropeMainWidget::on_asteropeCameraButton_pressed()
{
    qDebug() << "Camera settings button pressed";
    emit asteropeMainMenuButton_pressed(ASTEROPE_CAMERA);
}

void AsteropeMainWidget::on_asteropeAssistantsButton_pressed()
{
    qDebug() << "Assistants button pressed";
    emit asteropeMainMenuButton_pressed(ASTEROPE_ASSISTANTS);
}


void AsteropeMainWidget::on_asteropeSequenceButton_pressed()
{
    qDebug() << "Sequence button pressed";
    emit asteropeMainMenuButton_pressed(ASTEROPE_SEQUENCE);
}

void AsteropeMainWidget::on_asteropeAboutUsButton_pressed()
{
    qDebug() << "About us button pressed";
    emit asteropeMainMenuButton_pressed(ASTEROPE_ABOUTUS);
}

void AsteropeMainWidget::on_asteropeExitButton_pressed()
{
    qDebug() << "Exit button pressed";
    emit asteropeMainMenuButton_pressed(ASTEROPE_EXIT);
}
