#ifndef ASTEROPECONST_H
#define ASTEROPECONST_H
#include <QString>

const QString BATTERY100_ICON    = ":/icons/common/battery_100.png";
const QString BATTERY_DISCONNECT = ":/icons/common/battery_disconnect.png";
const QString CONNECT_ICON       = ":/icons/common/connect_camera.png";
const QString DISCONNECT_ICON    = ":/icons/common/disconnect_camera.png";
const QString BACK_ICON          = ":/icons/common/back.png";
const QString HOME_ICON          = ":/icons/common/home.png";
const QString OFF_ICON           = ":/icons/common/off.png";
const QString LINUX_ICON         = ":/icons/common/linux.png";
const QString MSWINDOWS_ICON     = ":/icons/common/windows.png";
const QString MACOSX_ICON        = ":/icons/common/apple.png";
const QString GENERIC_OS_ICON    = ":/icons/common/noos.png";

#endif // ASTEROPECONST_H
