#ifndef ASTEROPE_H
#define ASTEROPE_H

#include <QMainWindow>
#include <QListWidgetItem>
#include "asteropemainwidget.h"
#include "about/asteropeaboutuswidget.h"
#include "settings/asteropesettingswidget.h"
#include "settings/settingslanguage.h"
#include "settings/settingsconfigure.h"
#include "settings/settingssequence.h"
#include "settings/settingsusbkey.h"
#include "settings/settingssetup.h"
#include "settings/settingsdiagnostics.h"
#include "camera/asteropecamerawidget.h"
#include "camera/asteropecamerawidget2.h"
#include "assistants/asteropeassistantswidget.h"
#include "assistants/asteropeassistantswidget2.h"
#include "sequence/asteropesequencewidget.h"
#include "../include/asteropeenum.h"
#include "asteropeblackwidget.h"

namespace Ui {
class Asterope;
}

class Asterope : public QMainWindow
{
    Q_OBJECT

public:
    explicit Asterope(QWidget *parent = nullptr);
    ~Asterope();
    void resizeEvent(QResizeEvent * /* event */);
    void showEvent(QShowEvent * /* event */);

private slots:
    void on_ctrlCmdListWidget_itemPressed(QListWidgetItem *item);
    void on_asteropeMainMenuButton_pressed(AsteropeSection section);
    void on_asteropeSettingsMenuButton_pressed(SettingsSection section);
    void on_menuStackedWidget_currentChanged(int arg1);
    void on_asteropeCameraMenuButton_pressed(CameraSection section);
    void on_asteropeAssistantsMenuButton_pressed(AssistantsSection section);

private:
    void initMainWindow();
    void initCtrlPanel();
    void initMenuPanel();

    QString getCtrlStatusMainWidgetStyle();
    QString getCtrlCmdMainWidgetStyle();

private:
    Ui::Asterope *ui = nullptr;
    AsteropeMainWidget* m_asteropeMainWidget = nullptr;
    AsteropeSettingsWidget* m_asteropeSettingsWidget = nullptr;
    SettingsLanguage* m_asteropeSettingsLanguage = nullptr;
    SettingsConfigure* m_asteropeSettingsConfigure = nullptr;
    SettingsSequence* m_asteropeSettingsSequence = nullptr;
    SettingsUsbKey* m_asteropeSettingsUsbKey = nullptr;
    SettingsSetup* m_asteropeSettingsSetup = nullptr;
    SettingsDiagnostics* m_asteropeSettingsDiagnostics = nullptr;
    AsteropeCameraWidget* m_asteropeCameraWidget = nullptr;
    AsteropeCameraWidget2* m_asteropeCameraWidget2 = nullptr;
    AsteropeAssistantsWidget* m_asteropeAssistantsWidget = nullptr;
    AsteropeAssistantsWidget2* m_asteropeAssistantsWidget2 = nullptr;
    AsteropeSequenceWidget* m_asteropeSequenceWidget = nullptr;
    AsteropeAboutUsWidget* m_asteropeAboutUsWidget = nullptr;
    AsteropeBlackWidget* m_asteropeBlackWidget = nullptr;
    int m_currentWidgetIdx = 0;
    std::vector<int> m_previousWidgetIdx;
};

#endif // ASTEROPE_H
