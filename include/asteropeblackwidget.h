#ifndef ASTEROPEBLACKWIDGET_H
#define ASTEROPEBLACKWIDGET_H

#include <QMainWindow>

namespace Ui {
class AsteropeBlackWidget;
}

class AsteropeBlackWidget : public QMainWindow
{
    Q_OBJECT

public:
    explicit AsteropeBlackWidget(QWidget *parent = nullptr);
    ~AsteropeBlackWidget();
    void resizeEvent(QResizeEvent * /* event */);
    void showEvent(QShowEvent * /* event */);

private:
    void initAsteropeBlackWidget();

private slots:
    void on_switchOnButton_pressed();

private:
    Ui::AsteropeBlackWidget *ui = nullptr;
};

#endif // ASTEROPEBLACKWIDGET_H
