#ifndef ASTEROPECAMERAWIDGET_H
#define ASTEROPECAMERAWIDGET_H

#include <QWidget>
#include "../../include/asteropeenum.h"

namespace Ui {
class AsteropeCameraWidget;
}

class AsteropeCameraWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AsteropeCameraWidget(QWidget *parent = nullptr);
    ~AsteropeCameraWidget();
    void resizeEvent(QResizeEvent * /* event */);
    void showEvent(QShowEvent * /* event */);

signals:
    void asteropeCameraMenuButton_pressed(CameraSection section);

private slots:
    void on_cameraNextButton_pressed();

private:
    void initAsteropeCameraMainWidget();
    QString getAsteropeCameraWidgetStyle();
    QString getAsteropeCameraButtonStyle();
    QString getAsteropeCameraLabelStyle();
    QString getAsteropeCameraCtrlButtonStyle();
    QString getAsteropeCameraCtrlLabelStyle();

private:
    Ui::AsteropeCameraWidget *ui;
};

#endif // ASTEROPECAMERAWIDGET_H
