#ifndef ASTEROPECAMERAWIDGET2_H
#define ASTEROPECAMERAWIDGET2_H

#include <QWidget>
#include "../../include/asteropeenum.h"

namespace Ui {
class AsteropeCameraWidget2;
}

class AsteropeCameraWidget2 : public QWidget
{
    Q_OBJECT

public:
    explicit AsteropeCameraWidget2(QWidget *parent = nullptr);
    ~AsteropeCameraWidget2();
    void resizeEvent(QResizeEvent * /* event */);
    void showEvent(QShowEvent * /* event */);

signals:
    void asteropeCameraMenuButton_pressed(CameraSection section);

private:
    void initAsteropeCameraMainWidget();
    QString getAsteropeCameraWidgetStyle();
    QString getAsteropeCameraButtonStyle();
    QString getAsteropeCameraLabelStyle();
    QString getAsteropeCameraCtrlButtonStyle();
    QString getAsteropeCameraCtrlLabelStyle();

private:
    Ui::AsteropeCameraWidget2 *ui;
};

#endif // ASTEROPECAMERAWIDGET2_H
