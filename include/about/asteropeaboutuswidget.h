#ifndef ASTEROPEABOUTUSWIDGET_H
#define ASTEROPEABOUTUSWIDGET_H

#include <QWidget>
#include "../../include/asteropeenum.h"

namespace Ui {
class AsteropeAboutUsWidget;
}

class AsteropeAboutUsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AsteropeAboutUsWidget(QWidget *parent = nullptr);
    ~AsteropeAboutUsWidget();
    void resizeEvent(QResizeEvent * /* event */);
    void showEvent(QShowEvent * /* event */);

signals:
    void asteropeAboutUsMenuButton_pressed(AboutUsSection section);

private:
    void initAsteropeAboutUsMainWidget();
    QString getAsteropeAboutUsWidgetStyle();
    QString getAsteropeAboutUsButtonStyle();
    QString getAsteropeAboutUsLabelStyle();

private:
    Ui::AsteropeAboutUsWidget *ui;
};

#endif // ASTEROPEABOUTUSWIDGET_H
