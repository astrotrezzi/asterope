#ifndef ASTEROPEMAINWIDGET_H
#define ASTEROPEMAINWIDGET_H

#include <QWidget>
#include "../include/asteropeenum.h"

namespace Ui {
class AsteropeMainWidget;
}

class AsteropeMainWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AsteropeMainWidget(QWidget *parent = nullptr);
    ~AsteropeMainWidget();
    void resizeEvent(QResizeEvent * /* event */);
    void showEvent(QShowEvent * /* event */);

signals:
    void asteropeMainMenuButton_pressed(AsteropeSection section);

private slots:
    void on_asteropeSettingsButton_pressed();
    void on_asteropeCameraButton_pressed();
    void on_asteropeAssistantsButton_pressed();
    void on_asteropeSequenceButton_pressed();
    void on_asteropeAboutUsButton_pressed();
    void on_asteropeExitButton_pressed();

private:
    void initAsteropeMainWidget();
    QString getAsteropeMainWidgetStyle();
    QString getAsteropeButtonStyle(AsteropeSection section);
    QString getAsteropeLineStyle(AsteropeSection section);

private:
    Ui::AsteropeMainWidget *ui = nullptr;
};

#endif // ASTEROPEMAINWIDGET_H
