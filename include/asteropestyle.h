#ifndef ASTEROPESTYLE_H
#define ASTEROPESTYLE_H

#include <QString>
#include "../include/asteropeenum.h"

class AsteropeStyle
{

public:
    explicit AsteropeStyle(){}
    ~AsteropeStyle(){}

public:
    QString getSettingsWidgetStyle(const AsteropeSection section);
    QString getSettingsButtonStyle(const AsteropeSection section);
    QString getSettingsLabelStyle(const AsteropeSection section);
private:
};

#endif // ASTEROPESTYLE_H
