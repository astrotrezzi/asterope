#ifndef SETTINGSCONFIGURE_H
#define SETTINGSCONFIGURE_H

#include <QWidget>

namespace Ui {
class SettingsConfigure;
}

class SettingsConfigure : public QWidget
{
    Q_OBJECT

public:
    explicit SettingsConfigure(QWidget *parent = nullptr);
    ~SettingsConfigure();
    void resizeEvent(QResizeEvent * /* event */);
    void showEvent(QShowEvent * /* event */);

private:
    void initSettingsConfigureMainWidget();
    QString getAsteropeConfigureWidgetStyle();
    QString getAsteropeConfigureButtonStyle();
    QString getAsteropeConfigureLabelStyle();

private:
    Ui::SettingsConfigure *ui;
};

#endif // SETTINGSCONFIGURE_H
