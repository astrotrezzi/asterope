#ifndef SETTINGSUSBKEY_H
#define SETTINGSUSBKEY_H

#include <QWidget>

namespace Ui {
class SettingsUsbKey;
}

class SettingsUsbKey : public QWidget
{
    Q_OBJECT

public:
    explicit SettingsUsbKey(QWidget *parent = nullptr);
    ~SettingsUsbKey();
    void resizeEvent(QResizeEvent * /* event */);
    void showEvent(QShowEvent * /* event */);

private:
    void initSettingsUsbMainWidget();
    QString getAsteropeUsbWidgetStyle();
    QString getAsteropeUsbButtonStyle();
    QString getAsteropeUsbLabelStyle();

private:
    Ui::SettingsUsbKey *ui;
};

#endif // SETTINGSUSBKEY_H
