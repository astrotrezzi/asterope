#ifndef SETTINGSSEQUENCE_H
#define SETTINGSSEQUENCE_H

#include <QWidget>

namespace Ui {
class SettingsSequence;
}

class SettingsSequence : public QWidget
{
    Q_OBJECT

public:
    explicit SettingsSequence(QWidget *parent = nullptr);
    ~SettingsSequence();
    void resizeEvent(QResizeEvent * /* event */);
    void showEvent(QShowEvent * /* event */);

private:
    void initSettingsSequenceMainWidget();
    QString getAsteropeSettingsSequenceWidgetStyle();
    QString getAsteropeSettingsSequenceButtonStyle();
    QString getAsteropeSettingsSequenceLabelStyle();

private:
    Ui::SettingsSequence *ui;
};

#endif // SETTINGSSEQUENCE_H
