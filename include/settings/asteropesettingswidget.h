#ifndef ASTEROPESETTINGSWIDGET_H
#define ASTEROPESETTINGSWIDGET_H

#include <QWidget>
#include "../../include/asteropeenum.h"

namespace Ui {
class AsteropeSettingsWidget;
}

class AsteropeSettingsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AsteropeSettingsWidget(QWidget *parent = nullptr);
    ~AsteropeSettingsWidget();
    void resizeEvent(QResizeEvent * /* event */);
    void showEvent(QShowEvent * /* event */);

signals:
    void asteropeSettingsMenuButton_pressed(SettingsSection section);

private slots:
    void on_settingsLanguageButton_pressed();
    void on_settingsConfigureButton_pressed();
    void on_settingsSequenceButton_pressed();
    void on_settingsUsbButton_pressed();
    void on_settingsSetupButton_pressed();
    void on_settingsDiagnosticsButton_pressed();

private:
    void initAsteropeSettingsMainWidget();
    QString getAsteropeSettingsWidgetStyle();
    QString getAsteropeSettingsButtonStyle();
    QString getAsteropeSettingsLabelStyle();

private:
    Ui::AsteropeSettingsWidget *ui = nullptr;
};

#endif // ASTEROPESETTINGSWIDGET_H
