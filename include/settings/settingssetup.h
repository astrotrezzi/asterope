#ifndef SETTINGSSETUP_H
#define SETTINGSSETUP_H

#include <QWidget>

namespace Ui {
class SettingsSetup;
}

class SettingsSetup : public QWidget
{
    Q_OBJECT

public:
    explicit SettingsSetup(QWidget *parent = nullptr);
    ~SettingsSetup();
    void resizeEvent(QResizeEvent * /* event */);
    void showEvent(QShowEvent * /* event */);

private:
    void initSettingsSetupMainWidget();
    QString getAsteropeSetupWidgetStyle();
    QString getAsteropeSetupButtonStyle();
    QString getAsteropeSetupLabelStyle();

private:
    Ui::SettingsSetup *ui;
};

#endif // SETTINGSSETUP_H
