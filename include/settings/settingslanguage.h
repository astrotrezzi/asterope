#ifndef SETTINGSLANGUAGE_H
#define SETTINGSLANGUAGE_H

#include <QWidget>

namespace Ui {
class SettingsLanguage;
}

class SettingsLanguage : public QWidget
{
    Q_OBJECT

public:
    explicit SettingsLanguage(QWidget *parent = nullptr);
    ~SettingsLanguage();
    void resizeEvent(QResizeEvent * /* event */);
    void showEvent(QShowEvent * /* event */);

private:
    void initSettingsLanguageMainWidget();
    QString getAsteropeLanguageWidgetStyle();
    QString getAsteropeLanguageButtonStyle();
    QString getAsteropeLanguageLabelStyle();

private:
    Ui::SettingsLanguage *ui;
};

#endif // SETTINGSLANGUAGE_H
