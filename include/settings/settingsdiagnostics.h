#ifndef SETTINGSDIAGNOSTICS_H
#define SETTINGSDIAGNOSTICS_H

#include <QWidget>

namespace Ui {
class SettingsDiagnostics;
}

class SettingsDiagnostics : public QWidget
{
    Q_OBJECT

public:
    explicit SettingsDiagnostics(QWidget *parent = nullptr);
    ~SettingsDiagnostics();
    void resizeEvent(QResizeEvent * /* event */);
    void showEvent(QShowEvent * /* event */);

private:
    void initSettingsDiagnosticsMainWidget();
    QString getAsteropeSettingsDiagnosticsWidgetStyle();
    QString getAsteropeSettingsDiagnosticsButtonStyle();
    QString getAsteropeSettingsDiagnosticsLabelStyle();

private:
    Ui::SettingsDiagnostics *ui;
};

#endif // SETTINGSDIAGNOSTICS_H
