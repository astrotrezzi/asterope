#ifndef ASTEROPESEQUENCEWIDGET_H
#define ASTEROPESEQUENCEWIDGET_H

#include <QWidget>

namespace Ui {
class AsteropeSequenceWidget;
}

class AsteropeSequenceWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AsteropeSequenceWidget(QWidget *parent = nullptr);
    ~AsteropeSequenceWidget();

private:
    QString getAsteropeSequenceWidgetStyle();

private:
    Ui::AsteropeSequenceWidget *ui;
};

#endif // ASTEROPESEQUENCEWIDGET_H
