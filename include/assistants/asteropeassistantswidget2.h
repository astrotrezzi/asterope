#ifndef ASTEROPEASSISTANTSWIDGET2_H
#define ASTEROPEASSISTANTSWIDGET2_H

#include <QWidget>
#include "../../include/asteropeenum.h"

namespace Ui {
class AsteropeAssistantsWidget2;
}

class AsteropeAssistantsWidget2 : public QWidget
{
    Q_OBJECT

public:
    explicit AsteropeAssistantsWidget2(QWidget *parent = nullptr);
    ~AsteropeAssistantsWidget2();
    void resizeEvent(QResizeEvent * /* event */);
    void showEvent(QShowEvent * /* event */);

signals:
    void asteropeAssistantsMenuButton_pressed(AssistantsSection section);

private:
    void initAsteropeAssistantsMainWidget();
    QString getAsteropeAssistantsWidgetStyle();
    QString getAsteropeAssistantsButtonStyle();
    QString getAsteropeAssistantsLabelStyle();
    QString getAsteropeAssistantsCtrlButtonStyle();
    QString getAsteropeAssistantsCtrlLabelStyle();

private:
    Ui::AsteropeAssistantsWidget2 *ui;
};

#endif // ASTEROPEASSISTANTSWIDGET2_H
