#ifndef ASTEROPEASSISTANTSWIDGET_H
#define ASTEROPEASSISTANTSWIDGET_H

#include <QWidget>
#include "../../include/asteropeenum.h"

namespace Ui {
class AsteropeAssistantsWidget;
}

class AsteropeAssistantsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AsteropeAssistantsWidget(QWidget *parent = nullptr);
    ~AsteropeAssistantsWidget();
    void resizeEvent(QResizeEvent * /* event */);
    void showEvent(QShowEvent * /* event */);

signals:
    void asteropeAssistantsMenuButton_pressed(AssistantsSection section);

private slots:
    void on_assistantsNextButton_pressed();

private:
    void initAsteropeAssistantsMainWidget();
    QString getAsteropeAssistantsWidgetStyle();
    QString getAsteropeAssistantsButtonStyle();
    QString getAsteropeAssistantsLabelStyle();
    QString getAsteropeAssistantsCtrlButtonStyle();
    QString getAsteropeAssistantsCtrlLabelStyle();

private:
    Ui::AsteropeAssistantsWidget *ui;
};

#endif // ASTEROPEASSISTANTSWIDGET_H
